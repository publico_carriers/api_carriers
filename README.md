[![N|Solid](https://www.carriers.com.br/site/img/logos/logo.png)](https://nodesource.com/products/nsolid)
# API CARRIERS
____
API para cadastros e consultas de pedidos, verficações de trackings e fretes.

# Requisição
> Homologacao: https://clienthmg.carriers.com.br/

> Producao: http://api.carriers.com.br/client/

```JSON
 curl -X GET \
   https://clienthmg.carriers.com.br/ \
  -H 'Authorization: Bearer <API-KEY>' \
  -H 'cache-control: no-cache'
  ```

* Cadastro de pedidos:
    * [Cadastro de pedidos](ConsultaPedidos/CadastroPedidos.md)
    * [Cadastro de declaração de conteúdo/serviços](ConsultaPedidos/CadastroServicos.md)

* Consulta Tracking:
    * [Consulta de Tracking](ConsultaPedidos/ConsultaTracking.md)
    * [Consulta de Tracking V2](ConsultaPedidos/ConsultaTrackingV2.md)
    * [Consulta de Tracking V3](ConsultaPedidos/ConsultaTrackingV3.md)
    * [Webhook](ConsultaPedidos/WebHook.md)

* Consulta Frete:
    * [Consulta de Frete](ConsultaPedidos/ConsultaFrete.md)
    * [Consulta de Frete por volume](ConsultaPedidos/ConsultaFreteVolume.md)

* Consulta CTE:
    * [Consulta de CTE](ConsultaPedidos/ConsultaCte.md)

* Consulta Etiqueta:    
    * [Consulta de Etiqueta](ConsultaPedidos/ConsultaEtiqueta.md)

* Smart Label:
    * [Smart Label(Etiqueta Inteligente)](ConsultaPedidos/SmartLabel.md)
