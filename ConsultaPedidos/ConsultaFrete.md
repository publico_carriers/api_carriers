# Introdução
A API para consultar frete


### Solicitação POST
> /Carriers/Rates/{cep}

# Parâmetros (body json)
|Campo|Descricao|Obrigatório|Tipo|Obs             
|----------------|---------------|---------------|---------------|---------------|
ValorDeclarado|Valor do produto |sim|Numeric|valor minimo 0
peso|Peso do produto|sim|Numeric|valor minimo 0
altura|Altura da embalagem|sim|Numeric|valor minimo 0
largura|Largura da embalagem|sim|Numeric|valor minimo 0
comprimento|comprimento da embalagem|sim|Numeric| valor minimo 0


```JSON
`curl -X POST \
  {host}/Carriers/Rates/06385843 \
  -H 'Authorization: Bearer API-KEY' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{"ValorDeclarado":200.00,"peso": 20, "altura": 100, "largura": 10, "comprimento": 10}'
  ```

### Códigos de erro 

Erros de sintaxe ou inesperados (erro 500)
```JS
{
    "status": "erro",
    "errors": {
        "servidor": [
            "Erro no servidor"
        ]
    }
}
```

Erros de Cep

```JS
{
    "status": "erro",
    "errors": {
        "cep": [
            "Cep Não atendido"
        ]
    }
}
```

```JS
{
    "status": "erro",
    "errors": {
        "cep": [
            "Cep Não encontrado"
        ]
    }
}
```


Erros de requisição (erro 400)


```JS
{
    "status": "erro",
    "errors": {
        "ValorDeclarado": [
            "campo ValorDeclarado invalido"
        ],
        "peso": [
            "campo peso invalido"
        ],
        "largura": [
            "campo largura invalido"
        ],
        "altura": [
            "campo altura invalido"
        ],
        "comprimento": [
            "campo comprimento invalido"
        ],
        "cep": [
            "campo cep invalido"
        ]
    }
}
```



## Resposta

```JS
{
    "status": "sucesso",
    "cep": "06385843",
    "frete": [
        {
            "modalidade": "NORMAL",
            "prazo": 4,
            "preco": 53.31
        }
    ]
}
```
