# Introdução
A API para consultar pedidos cte


### Solicitação Post
> /Carriers/ConsultaCte

# Parâmetros (body json)
|Campo|Descricao|Obrigatório|Tipo|Obs             
|----------------|---------------|---------------|---------------|---------------|
pedido| Número do pedido|sim|Array de Objeto|
tipo|Tipo do Pedido|sim|String| chaveNota ou pedidoCliente

## Códigos de erro

Erros de requisição (erro 400)
```JS
{
    "status": false,
    "dados": "Tipo do pedido não informado."
}
```

# Parâmetros (body json)
|Campo|Descricao|        
|----------------|---------------|
totalPedidoEnviados| Total de pedidos enviados
pedidosEncontrados| Total De pedidos encontrados
pedidosNaoEncontrados| Total de pedidos não encontrados
pedidos| Dados dos pedidos

```JS
{
    "dados": {
        "totalPedidoEnviados": 1,
        "pedidosEncontrados": 1,
        "pedidosNaoEncontrados": 0,
        "pedidos": [
            {
                "PedidoCliente": "87364849284",
                "ChaveNota": "92525000011521010412601672893001335821911150",
                "ChaveCTE": "570001311186990060000966052007936952001",
                "TipoCTE": "cte_normal",
                "xmlEnviado": ""
            }
        ]
    }
}
```
