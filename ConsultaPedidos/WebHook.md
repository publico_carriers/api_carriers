# Introdução
Webhook envio de ocorrências.


```JS
{
    “headers”:{
        "Content-Type": "application/json"
    }
}
```

# Parâmetros
|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
Transportadora|Nome da transportadora |sim|String
Transportadora_cnpj|CNPJ da transportadora |sim|Numeric
Embarcador| Nome do embarcador |sim|String
Embarcador_cnpj| CNPJ do embarcador |sim|Numeric
NFe| Chave nota fiscal  |sim|String
serieNF| Série nota fiscal  |sim|String
NotaFiscal| Número nota fiscal  |sim|String
PedidoCliente|Id de identificação do pedido cliente|sim|Numeric
dtOcorrencia|Data da ocorrência Ex: Ano-Mês-Dia Horas:Minutos:Segundos|sim|String
idOcorrencia|Id status da ocorrência|sim|Numeric
Ocorrencia|Descrição da ocorrência|sim|String
localizacao|Origem da requisição|sim| Objeto localizacao


# Objeto localizacao
|Campo|Descricao|Obs|Tipo             
|----------------|---------------|---------------|---------------|
latitude|Latitude da ocorencia|Se não houver latitude e longitude será enviado o CEP|String
longitude|Longitude da ocorencia|Se não houver latitude e longitude será enviado o CEP|String
cep|cep|Se não houver latitude será enviado o CEP|String

### Objeto **Json**

## Exemplo de requisição

```JS
{
    "Transportadora": “Carriers”,
    "Transportadora_cnpj": “16458754852”,
    "Embarcador": “João Destinatário”,
    "Embarcador_cnpj": “17329479020”,
    "NFe":“12345678901234567890123456789012345678901234”,
    "serieNF": “12345678901234567890123456789012345678”,
    "NotaFiscal": "1245",
    "PedidoCliente": “64687150”,
    "dtOcorrencia":“2024-10-24 15:25:01”,
    "idOcorrencia": “22”,
    "Ocorrencia": ”Coletado”,
    "localizacao":{
        "latitude":"42.000055",
        "longitude":"41.00036",
        "cep":1845298
    }
}
```

## Retorno esperado com sucesso

```JS
{
    “code”:200,
    “message”:”ok”,
    "idRequisicao": 1525
}
```

### Códigos de erro 
Erros de requisição (erro 400)
```JS
{
    “code”:400,
    “message”:”erro”,
    "idRequisicao": 1524
}
```

|Campo|Descricao|Obrigatório|Tipo             
|----------------|---------------|---------------|---------------|
code| Código da requisição |sim|Numeric
message| Mensagem de retorno |sim|String
idRequisicao| Precisa ser um id único para casa envio recebido. |sim|String

