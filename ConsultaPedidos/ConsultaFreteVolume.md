# Introdução
A API para consultar frete por volume  


### Solicitação POST
> /Carriers/Rates/Packages/{cep}

# Parâmetros (body json)
|Campo|Descricao|Obrigatório|Tipo|Obs             
|----------------|---------------|---------------|---------------|---------------|
ValorDeclarado|Valores dos produtos |sim|Numeric|valor minimo 0
Volumes|Volumes do Pedidos|sim|Array de Objeto| 

### Objeto **Volumes**
|Campo|Descricao|Obrigatório|Tipo|Obs             
|----------------|---------------|---------------|---------------|---------------|
ValorDeclarado|Valor do produto |sim|Numeric|valor minimo 0
Peso|Peso do produto|sim|Numeric|valor minimo 0
Altura|Altura da embalagem|sim|Numeric|valor minimo 0
Largura|Largura da embalagem|sim|Numeric|valor minimo 0
Comprimento|comprimento da embalagem|sim|Numeric| valor minimo 0



```JSON
`curl -X POST \
  {host}/Carriers/Rates/Packages/{cep} \
  -H 'Authorization: Bearer {API-KEY}' \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{"ValorDeclarado":77,"Volumes":[{"Peso":20,"Altura":50,"Largura":10,"Comprimento":10},{"Peso":20,"Altura":50,"Largura":10,"Comprimento":10},{"Peso":20,"Altura":50,"Largura":10,"Comprimento":10}]}'
  ```

### Códigos de erro 

Erros de sintaxe ou inesperados (erro 500)
```JS
{
    "status": "erro",
    "errors": {
        "servidor": [
            "Erro no servidor"
        ]
    }
}
```

Erros de Cep

```JS
{
    "status": "erro",
    "errors": {
        "cep": [
            "Cep Não atendido"
        ]
    }
}
```

```JS
{
    "status": "erro",
    "errors": {
        "cep": [
            "Cep Não encontrado"
        ]
    }
}
```


Erros de requisição (erro 400)


```JS
{
    "status": "erro",
    "errors": {
        "ValorDeclarado": [
            "O campo ValorDeclarado na posição {posição} é invalido"
        ],
        "peso": [
            "O campo na posição {posição} é peso invalido"
        ],
        "largura": [
            "O campo largura na posição {posição} é invalido"
        ],
        "altura": [
            "O campo altura  na posição {posição} éinvalido"
        ],
        "comprimento": [
            "O campo comprimento na posição {posição} é invalido"
        ],
        "cep": [
            "O campo cep na posição {posição} é invalido"
        ]
    }
}
```



## Resposta

```JS
{
    "status": "sucesso",
    "cep": "06385843",
    "frete": [
        {
            "modalidade": "NORMAL",
            "prazo": 4,
            "preco": 53.31
        }
    ]
}
```
